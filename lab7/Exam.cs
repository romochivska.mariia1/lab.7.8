﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace lab5
{
    public class Exam
    {
        [Key]
        public int Id { get; set; }

        public string LastName { get; set; }

        public string GroupName { get; set; }

        public string Subject { get; set; }

        public int TicketNumber { get; set; }

        public int Grade { get; set; }

        public string Teacher { get; set; }
    }
}

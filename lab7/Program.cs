﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace lab5
{
    internal class Program
    {
        static async Task Main(string[] args)
        {
            Console.WriteLine("Лабораторна робота 7: Введіть номер запиту (1-14), або 0 для завершення:");

            while (true)
            {
                Console.Write("Введіть номер запиту: ");
                if (int.TryParse(Console.ReadLine(), out int choice))
                {
                    if (choice == 0)
                    {
                        Console.WriteLine("Програма завершена.");
                        break;
                    }

                    await ВиконатиЗапитAsync(choice);
                }
                else
                {
                    Console.WriteLine("Некоректний ввід. Введіть числове значення.");
                }
            }
        }

        static async Task ВиконатиЗапитAsync(int choice)
        {
            using (var context = new ApplicationContext())
            {
                switch (choice)
                {
                    case 1:
                        await ПростийЗапитAsync(context);
                        break;
                    case 2:
                        Console.Write("Введіть частину предмету для пошуку: ");
                        string subjectInput = Console.ReadLine();
                        await СпеціальнийФункціональнийЗапитAsync(context, "LIKE", "Subject", $"%{subjectInput}%");
                        break;
                    case 3:
                        await СкладнийКритерійЗапитAsync(context);
                        break;
                    case 4:
                        await УнікальніЗначенняЗапитAsync(context, "GroupName");
                        break;
                    case 5:
                        await ПростийЗапитAsync(context, "Grade >= 80 AND Grade <= 90");
                        break;
                    case 6:
                        await СпеціальнийФункціональнийЗапитAsync(context, ">", "Grade", "90 AND GroupName = 'GroupA'");
                        break;
                    case 7:
                        await УнікальніЗначенняЗапитAsync(context, "GroupName");
                        break;
                    case 8:
                        await ОбчисленеПоляЗапитAsync(context);
                        break;
                    case 9:
                        await ГрупувальнийЗапитAsync(context, "GroupName");
                        break;
                    case 10:
                        await СортувальнийЗапитAsync(context, "LastName", "ASC");
                        break;
                    case 11:
                        await СортувальнийЗапитAsync(context, "LastName", "DESC");
                        break;
                    case 12:
                        await ЗмінаЗапитAsync(context, "Grade", "88", "LastName", "John Doe");
                        break;
                    case 13:
                        await СпеціальнийФункціональнийЗапитAsync(context, "=", "LastName", "John Doe");
                        break;
                    case 14:
                        await НовийЗапитAsync(context);
                        break;
                    default:
                        Console.WriteLine("Некоректний вибір. Введіть номер від 1 до 14, або 0 для завершення.");
                        break;
                }
            }
        }

        static async Task ПростийЗапитAsync(ApplicationContext context, string additionalCondition = null)
        {
            var query = await context.Exams.Select(x => new
            {
                LastName = x.LastName
            }).Distinct().ToListAsync();

            await ВивестиРезультатиAsync(query);
        }

        static async Task СпеціальнийФункціональнийЗапитAsync(ApplicationContext context, string functionName, string columnName, string value)
        {
            var query = await context.Exams.Where(x => EF.Functions.Like(x.Subject, value)).ToListAsync();
            await ВивестиРезультатиAsync(query);
        }

        static async Task СкладнийКритерійЗапитAsync(ApplicationContext context)
        {
            var query = await context.Exams.Where(x => x.Grade > 90 && x.GroupName == "GroupA").ToListAsync();
            await ВивестиРезультатиAsync(query);
        }

        static async Task УнікальніЗначенняЗапитAsync(ApplicationContext context, params string[] columnNames)
        {
            var query = await context.Exams.Select(x => new
            {
                GroupName = x.GroupName,
            }).Distinct().ToListAsync();

            await ВивестиРезультатиAsync(query);
        }

        static async Task ОбчисленеПоляЗапитAsync(ApplicationContext context)
        {
            var query = await context.Exams.Select(x => new { x.LastName, x.Grade, DoubledGrade = x.Grade * 2 }).ToListAsync();
            await ВивестиРезультатиAsync(query);
        }

        static async Task ГрупувальнийЗапитAsync(ApplicationContext context, string groupByColumn)
        {
            var query = await context.Exams.GroupBy(x => x.GroupName).Select(g => new { GroupName = g.Key, AverageGrade = g.Average(x => x.Grade) }).ToListAsync();
            await ВивестиРезультатиAsync(query);
        }

        static async Task СортувальнийЗапитAsync(ApplicationContext context, string orderByColumn, string sortOrder)
        {
            var query = await (sortOrder == "ASC"
                ? context.Exams.OrderBy(x => EF.Property<object>(x, orderByColumn)).ToListAsync()
                : context.Exams.OrderByDescending(x => EF.Property<object>(x, orderByColumn)).ToListAsync());

            await ВивестиРезультатиAsync(query);
        }

        static async Task ЗмінаЗапитAsync(ApplicationContext context, string columnName, string newValue, string conditionColumn, string conditionValue)
        {
            var exam = await context.Exams.FirstOrDefaultAsync(x => EF.Property<object>(x, conditionColumn).ToString() == conditionValue);
            if (exam != null)
            {
                exam.Grade = int.Parse(newValue);
                await context.SaveChangesAsync();
                Console.WriteLine($"Кількість змінених рядків: 1");
            }
            else
            {
                Console.WriteLine($"Рядок з умовою {conditionColumn} = {conditionValue} не знайдено.");
            }
        }

        static async Task НовийЗапитAsync(ApplicationContext context)
        {
            var exam = await context.Exams.FirstOrDefaultAsync(x => x.Grade > 90 && x.GroupName == "GroupA");

            if (exam != null)
            {
                Console.WriteLine($"Результат запиту: {exam}");
            }
            else
            {
                Console.WriteLine("Рядок не знайдено.");
            }
        }

        static async Task ВивестиРезультатиAsync<T>(List<T> results)
        {
            Console.WriteLine("Результат запиту:");
            foreach (var result in results)
            {
                Console.WriteLine(result);
            }
            Console.WriteLine();
        }
    }
}
